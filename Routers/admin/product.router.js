const express = require('express')
const router = express.Router()
const Products = require('../../Controllers/admin/product.controller')
const { isAutheticatedUser } = require('../../Middlewares/auth')

router.get('/all', Products.getAllProduct)

router.get('/', isAutheticatedUser, Products.index)

router.get('/:id', isAutheticatedUser, Products.details)

router.get('/total', isAutheticatedUser, Products.amountProduct)

router.post('/create', isAutheticatedUser, Products.create)

router.patch('/update', isAutheticatedUser, Products.update)

router.delete('/delete', isAutheticatedUser, Products.delete)


module.exports = router