const express = require('express')
const router = express.Router()
const Sale = require('../../Controllers/admin/sale.controller')
const { isAutheticatedUser } = require('../../Middlewares/auth')

router.get('/', isAutheticatedUser, Sale.index)

router.post('/', isAutheticatedUser, Sale.create)

router.get('/:id', isAutheticatedUser, Sale.detail)

router.patch('/:id', isAutheticatedUser, Sale.update)

router.get('/list/product', Sale.list)

router.get('/list/:id', Sale.detailList)

module.exports = router