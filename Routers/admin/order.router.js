const express = require('express')

const router = express.Router()

const Order = require('../../Controllers/admin/order.controller')
const { isAutheticatedUser } = require('../../Middlewares/auth')

router.get('/', isAutheticatedUser, Order.index)
router.get('/detail/:id', isAutheticatedUser, Order.details)
router.get('/detailorder/:id', isAutheticatedUser, Order.detailOrder)

router.patch('/confirmorder', isAutheticatedUser, Order.confirmOrder)
router.patch('/cancelorder', Order.cancelOrder)
router.patch('/delivery', isAutheticatedUser, Order.delivery)
router.patch('/confirmdelivery', isAutheticatedUser, Order.confirmDelivery)

router.get('/completeOrder', isAutheticatedUser, Order.completeOrder)
router.get('/income', isAutheticatedUser, Order.getMonthlyIncome)
router.get('/income/all', Order.getsIncome)
router.get('/all', Order.getAllOrder)
router.get('/customer-vip', Order.getsVipCustomer)
router.get('/product-vip', Order.getsProductTop)

module.exports = router