const express = require('express')
const router = express.Router()
const Category = require('../../Controllers/admin/category.controller')
const { isAutheticatedUser } = require('../../Middlewares/auth')

router.get('/', isAutheticatedUser, Category.index)

router.get('/:id', isAutheticatedUser, Category.detail)

router.get('/detail/:id', isAutheticatedUser, Category.detailProduct)

router.post('/create', isAutheticatedUser, Category.create)

router.delete('/delete', isAutheticatedUser, Category.delete)

router.put('/update', isAutheticatedUser, Category.update)

module.exports = router