const express = require('express')
const router = express.Router()
const User = require('../../Controllers/admin/user.controller')
const { isAutheticatedUser } = require('../../Middlewares/auth')

router.get('/', User.index)

router.get('/:id', isAutheticatedUser, User.details)

router.post('/create', User.create)

router.post('/login', User.login)

router.patch('/update', isAutheticatedUser, User.update)

router.delete('/delete', isAutheticatedUser, User.delete)


module.exports = router