const express = require('express')
const router = express.Router()
const Coupon = require('../../Controllers/admin/coupon.controller')
const { isAutheticatedUser } = require('../../Middlewares/auth')

router.get('/', Coupon.index)

router.get('/:id', Coupon.detail)

router.post('/', isAutheticatedUser, Coupon.create)

router.patch('/:id', isAutheticatedUser, Coupon.update)

router.delete('/:id', isAutheticatedUser, Coupon.delete)

router.get('/promotion/checking', Coupon.checking)

router.patch('/promotion/:id', isAutheticatedUser, Coupon.createCoupon)

module.exports = router