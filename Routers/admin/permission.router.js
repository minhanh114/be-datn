const express = require('express')
const router = express.Router()
const Permission = require('../../Controllers/admin/permission.controller')
const { isAutheticatedUser } = require('../../Middlewares/auth')

router.get('/', isAutheticatedUser, Permission.index)
router.get('/all', isAutheticatedUser, Permission.all)
router.get('/:id', isAutheticatedUser, Permission.details)

router.post('/create', isAutheticatedUser, Permission.create)

router.delete('/delete', isAutheticatedUser, Permission.delete)
router.put('/update', isAutheticatedUser, Permission.update)


module.exports = router