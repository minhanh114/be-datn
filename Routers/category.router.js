const express = require('express')

const router = express.Router()

const Category = require('../Controllers/category.controller')

router.get('/', Category.index)


module.exports = router