const express = require('express')

const router = express.Router()

const Comment = require('../Controllers/comment.controller')

router.get('/:id', Comment.index)

router.post('/:id', Comment.post_comment)

module.exports = router