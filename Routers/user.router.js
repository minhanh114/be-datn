const express = require('express')

const router = express.Router()

const Users = require('../Controllers/user.controller')

router.get('/', Users.index)

router.get('/:id', Users.user)

router.put('/', Users.update_user)

router.get('/detail/login', Users.detail)

router.post('/', Users.post_user)

router.post('/forgot-password', Users.forgotPass)

router.post('/reset-password', Users.resetPass)

module.exports = router