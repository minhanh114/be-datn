const express = require('express')

const router = express.Router()

const Note = require('../Controllers/note.controller')

router.post('/', Note.post_delivery)

router.get('/:id', Note.get_delivery)

module.exports = router