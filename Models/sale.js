const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        promotion: Number,
        describe: String,
        status: Boolean,
        start: Date,
        end: Date,
        id_product: {
            type: String,
            ref: 'Products'
        }
    },
    {
        timestamps: true,
    }
);

const Sale = mongoose.model('Sale', schema, 'sale');

module.exports = Sale;