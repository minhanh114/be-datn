const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        id_product: String,
        id_user: {
            type: String,
            ref: 'Users'
        },
        content: String,
        star: Number,
    },
    {
        timestamps: true,
    }
);

const Comment = mongoose.model('Comment', schema, 'comment');

module.exports = Comment;