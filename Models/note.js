const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        fullname: String,
        phone: String,
    },
    {
        timestamps: true,
    }
);

const Note = mongoose.model('Note', schema, 'note');

module.exports = Note;