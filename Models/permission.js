const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        permission: String,
        describe: String,
    },
    {
        timestamps: true,
    }
);

const Permission = mongoose.model('Permission', schema, 'permission');

module.exports = Permission;