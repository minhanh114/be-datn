const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        id_permission: {
            type: String,
            ref: 'Permission'
        },
        username: String,
        password: String,
        fullname: String,
        email: String,
        phone: String,
        coverted_Id: String,
        token: {
            type: String,
            default: null
        }
    },
    {
        timestamps: true,
    }
);

schema.pre('save', function (next) {
    this.coverted_Id = this._id.toString()
    next();
})
const Users = mongoose.model('Users', schema, 'user');

module.exports = Users;