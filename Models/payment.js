const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        pay_name: String
    }
);

const Payment = mongoose.model('Payment', schema, 'payment');

module.exports = Payment;