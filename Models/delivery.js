const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        id_delivery: String,
        from: String,
        to: String,
        distance: String,
        duration: String,
        price: String
    },
    {
        timestamps: true,
    }
);

const Delivery = mongoose.model('Delivery', schema, 'delivery');

module.exports = Delivery;