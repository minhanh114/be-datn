const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        id_user: {
            type: String,
            ref: 'Users'
        },
        id_product: {
            type: String.apply,
            ref: 'Products'
        }
    },
    {
        timestamps: true,
    }
);

const Favorite = mongoose.model('Favorite', schema, 'favorite');

module.exports = Favorite;