const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        code: String,
        count: Number,
        promotion: String,
        describe: String,
        users: {
            type: [String]
        }
    },
    {
        timestamps: true,
    }
);

const Coupon = mongoose.model('Coupon', schema, 'coupon');

module.exports = Coupon;