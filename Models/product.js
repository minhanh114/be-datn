const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        id_category: {
            type: String,
            ref: 'Category'
        },
        name_product: String,
        price_product: String,
        image: String,
        describe: String,
        /* gender: String, */
        number: {
            type: Number,
            default: 0
        },
        lastCountPay: Number
    },
    {
        timestamps: true,
    }
);

const Products = mongoose.model('Products', schema, 'product');

module.exports = Products;