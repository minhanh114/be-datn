const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        category: String,
        describe: String,
        gender: String
    }
);

const Category = mongoose.model('Category', schema, 'category');

module.exports = Category;