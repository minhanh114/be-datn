const express = require('express')
const app = express();
const cors = require("cors");
const cookieParser = require('cookie-parser')
const paypal = require('paypal-rest-sdk');// initialize paypal
const upload = require('express-fileupload');
const morgan = require('morgan');
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const dotenv = require('dotenv');
const http = require('http').Server(app);
const io = require('socket.io')(http);

const ProductAPI = require('./Routers/product.router')
const UserAPI = require('./Routers/user.router')
const OrderAPI = require('./Routers/order.router')
const Detail_OrderAPI = require('./Routers/detail_order.router')
const CommentAPI = require('./Routers/comment.router')
const CategoryAPI = require('./Routers/category.router')
const NoteAPI = require('./Routers/note.router')

const ProductAdmin = require('./Routers/admin/product.router')
const CategoryAdmin = require('./Routers/admin/category.router')
const Permission = require('./Routers/admin/permission.router')
const UserAdmin = require('./Routers/admin/user.router')
const Order = require('./Routers/admin/order.router')
const Coupon = require('./Routers/admin/coupon.router')
const Sale = require('./Routers/admin/sale.router')

// Setting up config file 
if (process.env.NODE_ENV !== 'PRODUCTION') require('dotenv').config({ path: '.env' })
dotenv.config({ path: '.env' })

//mongoose.connect("mongodb+srv://minhhieu:ZPCKa1ZMZfiAOe7w@cluster0.hrvuv.mongodb.net/Clothes?retryWrites=true&w=majority", {
mongoose.connect(process.env.DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  autoIndex: false
}).then(() => {
  console.log('Connected to database ')
})
  .catch((err) => {
    console.error(`Error connecting to the database. \n${err}`);
  });

app.use('/', express.static('public'))
app.use(upload());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cookieParser())
app.use(morgan(':method :url :status :response-time ms :date[iso]'))
app.use(bodyParser.json());
app.use(cors({
  origin: "*"
}));

// Cài đặt config cho paypal
paypal.configure({
  'mode': 'sandbox', //sandbox or live
  'client_id': 'AfDEZ8mShXtbnzjAPgI9Fg1Fz1JyEB1yCWd6oyMOuA7N0nxi1NHlGQmo2WrPeyV5NsuWbQN8SbN7-9Wq', // Thông số này copy bên my account paypal
  'client_secret': 'AfDEZ8mShXtbnzjAPgI9Fg1Fz1JyEB1yCWd6oyMOuA7N0nxi1NHlGQmo2WrPeyV5NsuWbQN8SbN7-9Wq' // Thông số này cùng vậy
});

// const path = require('path')
// if (process.env.NODE_ENV === 'PRODUCTION') {
//   app.use(express.static(path.join(__dirname, '../frontend/build')))
//   app.get('*', (req, res) => {
//       res.sendFile(path.resolve(__dirname, '../frontend/build/index.html'))
//   })
// }
app.use('/api/Product', ProductAPI)
app.use('/api/User', UserAPI)
app.use('/api/Payment', OrderAPI)
app.use('/api/Comment', CommentAPI)
app.use('/api/Note', NoteAPI)
app.use('/api/DetailOrder', Detail_OrderAPI)
app.use('/api/Category', CategoryAPI)

app.use('/api/admin/Product', ProductAdmin)
app.use('/api/admin/Category', CategoryAdmin)
app.use('/api/admin/Permission', Permission)
app.use('/api/admin/User', UserAdmin)
app.use('/api/admin/Order', Order)
app.use('/api/admin/Coupon', Coupon)
app.use('/api/admin/Sale', Sale)


io.on("connection", (socket) => {
  console.log(`Có người vừa kết nối, socketID: ${socket.id}`);
  socket.on('send_order', (data) => {
    console.log(data)
    socket.broadcast.emit("receive_order", data);
  })
})

http.listen(process.env.PORT, () => {
  console.log('listening on *: ' + process.env.PORT);
});
// Handle Unhandled Promise rejections
process.on('unhandledRejection', err => {
  console.log(`ERROR: ${err.stack}`);
  console.log('Shutting down the server due to Unhandled Promise rejection');
  server.close(() => {
    process.exit(1)
  });
});

// Handle Uncaught exceptions
process.on('uncaughtException', err => {
  console.log(`ERROR: ${err.stack}`);
  console.log('Shutting down due to uncaught exception');
  process.exit(1)
})

