const Order = require('../../Models/order')
const Detail_History = require('../../Models/detail_order')
const Payment = require('../../Models/payment')
const Delivery = require('../../Models/delivery')
const Detail_Order = require('../../Models/detail_order')
const Products = require('../../Models/product')

module.exports.index = async (req, res) => {
    let page = parseInt(req.query.page) || 1;
    let money = 0;

    const status = req.query.status

    const perPage = parseInt(req.query.limit) || 8;

    let start = (page - 1) * perPage;
    let end = page * perPage;

    let orders
    if (status) {
        orders = await (await Order.find({ status: status }).populate('id_user').populate('id_payment').populate('id_note')).reverse();
    } else {
        orders = await (await Order.find().populate('id_user').populate('id_note').populate('id_payment')).reverse();
    }

    const totalPage = Math.ceil(orders.length / perPage);

    orders.map((value) => {
        money += Number(value.total);
    })

    res.json({
        orders: orders.slice(start, end),
        totalPage: totalPage,
        totalMoney: money
    })


}

module.exports.detailOrder = async (req, res) => {
    let page = parseInt(req.query.page) || 1;
    const keyWordSearch = req.query.search;

    const perPage = parseInt(req.query.limit) || 8;

    let start = (page - 1) * perPage;
    let end = page * perPage;

    const details = await Detail_History.find({ id_order: req.params.id }).populate('id_order').populate('id_product');

    const totalPage = Math.ceil(details.length / perPage);

    if (!keyWordSearch) {
        res.json({
            details: details.slice(start, end),
            totalPage: totalPage
        })
    } else {
        const newData = details.filter(value => {
            return value.name_product.toUpperCase().indexOf(keyWordSearch.toUpperCase()) !== -1 ||
                value.price_product.toUpperCase().indexOf(keyWordSearch.toUpperCase()) !== -1 ||
                value.count.toString().toUpperCase().indexOf(keyWordSearch.toUpperCase()) !== -1 ||
                value.size.toUpperCase().indexOf(keyWordSearch.toUpperCase()) !== -1
        })

        res.json({
            details: newData.slice(start, end),
            totalPage: totalPage
        })
    }
}

module.exports.details = async (req, res) => {
    const order = await Order.findOne({ _id: req.params.id }).populate('id_user').populate('id_payment').populate('id_note');

    res.json(order)

}

module.exports.confirmOrder = async (req, res) => {
    await Order.updateOne({ _id: req.query.id }, { status: "2" }, function (err, res) {
        if (err) return res.json({ msg: err });
    });
    res.json({ msg: "Thanh Cong" })
}

module.exports.delivery = async (req, res) => {
    await Order.updateOne({ _id: req.query.id }, { status: "3" }, function (err, res) {
        if (err) return res.json({ msg: err });
    });
    res.json({ msg: "Thanh Cong" })
}

module.exports.confirmDelivery = async (req, res) => {
    await Promise.all([
        Order.updateOne({ _id: req.query.id }, { status: "4", pay: true }, function (err, res) {
            if (err) return res.json({ msg: err });
        }),
        Detail_Order.updateMany({ id_order: req.query.id }, { isPaid: true }, function (error, res) {
            if (error) return res.json({ message: error })
        })
    ]);
    res.json({ msg: "Thanh Cong" })
}

module.exports.cancelOrder = async (req, res) => {
    try {
        const order_details = await Detail_Order.find({ id_order: req.query.id });

        for (const order_detail of order_details) {
            const product = await Products.findOne({ _id: order_detail.id_product });
            product.number = product.number + product.lastCountPay;
            await product.save();
        };

        await Order.updateOne({ _id: req.query.id }, { status: "5" }, function (err, res) {
            if (err) {
                return res.json({ msg: err });
            }
        });
        res.json({ msg: "Thanh Cong" });
    } catch (error) {
        res.send(error.message);
    };
};


module.exports.completeOrder = async (req, res) => {

    let page = parseInt(req.query.page) || 1;
    let money = 0;

    const getDate = req.query.getDate

    const perPage = parseInt(req.query.limit) || 8;

    let start = (page - 1) * perPage;
    let end = page * perPage;

    const orders = await (await Order.find({ status: '4' }).populate('id_user').populate('id_payment').populate('id_note')).reverse();

    if (!getDate) {

        const totalPage = Math.ceil(orders.length / perPage);

        orders.map((value) => {
            money += Number(value.total);
        })

        res.json({
            orders: orders.slice(start, end),
            totalPage: totalPage,
            totalMoney: money
        })

    } else {

        const newOrder = orders.filter(value => {
            return value.create_time.toString().indexOf(getDate.toString()) !== -1
        })

        const totalPage = Math.ceil(newOrder.length / perPage);

        newOrder.map((value) => {
            money += Number(value.total);
        })

        res.json({
            orders: newOrder.slice(start, end),
            totalPage: totalPage,
            totalMoney: money
        })

    }

}

module.exports.getMonthlyIncome = async (req, res, next) => {
    const today = new Date();
    const startOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);
    const endOfMonth = new Date(today.getFullYear(), today.getMonth() + 1, 1);
    try {
        let income = await Order.aggregate([
            {
                $match: {
                    pay: true,
                    createdAt: {
                        $gte: startOfMonth,
                        $lt: endOfMonth
                    },
                }
            },
            {
                $group: {
                    _id: { $month: '$createdAt' },
                    total: { $sum: "$total" },
                },
            },
        ]);
        res.status(200).json(income);
    } catch (err) {
        res.status(500).json(err);
    }
}

module.exports.getsIncome = async (req, res, next) => {
    try {
        let income = await Order.aggregate([
            {
                $match: {
                    pay: true,
                    createdAt: {
                        $gte: new Date(req.query.startDate),
                        $lt: new Date(req.query.endDate)
                    }
                }
            },
            {
                $group: {
                    _id: null,
                    total: { $sum: '$total' },
                }
            }
        ]);
        res.status(200).json(income);
    } catch (err) {
        res.status(500).json(err);
    }
}

module.exports.getAllOrder = async (req, res, next) => {
    try {
        let order = await Order.find({ pay: true });
        res.status(200).json(order);
    } catch (err) {
        res.status(500).json(err);
    }
}

module.exports.getsVipCustomer = async (req, res, next) => {
    try {
        /*  const today = new Date();
         const startOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);
         const endOfMonth = new Date(today.getFullYear(), today.getMonth() + 1, 1); */
        const order = await Order.aggregate([
            {
                $match: {
                    pay: true,
                    createdAt: {
                        $gte: new Date(req.query.startDate),
                        $lt: new Date(req.query.endDate)
                    },
                }
            },
            {
                $lookup: {
                    from: "user",
                    let: { userId: "$id_user" },
                    pipeline: [
                        { $match: { $expr: { $eq: ["$coverted_Id", "$$userId"] } } },
                        { $project: { fullname: 1 } },
                    ],
                    as: "infoUser",
                }
            },
            { $addFields: { infoUser: { $arrayElemAt: ["$infoUser", 0] } } },
            {
                $group: {
                    _id: "$id_user",
                    fullname: { $first: "$infoUser.fullname" },
                    total: { $sum: '$total' },
                }
            },
            { $sort: { total: -1 } }
        ]);
        const vipCustomer = [];
        for (let item = 0; item < order.length; item++) {
            vipCustomer.push({
                userId: order[item]._id,
                fullname: order[item].fullname,
                total: order[item].total
            });
            // Only retrieve 5 item
            if (item === 4) {
                break;
            }
        }
        res.status(200).json(vipCustomer);
    } catch (err) {
        res.status(500).json(err);
    }
}

module.exports.getsProductTop = async (req, res, next) => {
    try {
        // const today = new Date();
        // const startOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);
        // const endOfMonth = new Date(today.getFullYear(), today.getMonth() + 1, 1);
        const order = await Detail_Order.aggregate([
            {
                $match: {
                    isPaid: true,
                    createdAt: {
                        $gte: new Date(req.query.startDate),
                        $lt: new Date(req.query.endDate)
                    },
                }
            },
            {
                $group: {
                    _id: "$id_product",
                    name: { $first: "$name_product" },
                    total: { $sum: '$count' },
                }
            },
            { $sort: { total: -1 } }
        ]);
        const vipProduct = [];
        for (let item = 0; item < order.length; item++) {
            vipProduct.push({
                productId: order[item]._id,
                name: order[item].name,
                total: order[item].total
            });
            // Only retrieve 5 item
            if (item === 4) {
                break;
            }
        }
        res.status(200).json(vipProduct);
    } catch (err) {
        res.status(500).json(err);
    }
}
