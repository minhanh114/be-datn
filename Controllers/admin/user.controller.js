const User = require('../../Models/user')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Products = require('../../Models/product');

module.exports.index = async (req, res) => {
    let page = parseInt(req.query.page) || 1;
    const keyWordSearch = req.query.search;

    const perPage = parseInt(req.query.limit) || 8;
    const totalPage = Math.ceil(await User.countDocuments() / perPage);

    let start = (page - 1) * perPage;
    let end = page * perPage;
    let users;
    if (req.query.permission) {
        users = await User.find({ id_permission: req.query.permission }).populate('id_permission')
    } else {
        users = await User.find({}).populate('id_permission')
    }


    if (!keyWordSearch) {
        res.json({
            users: users.slice(start, end),
            totalPage: totalPage
        })

    } else {
        const newData = users.filter(value => {
            return value.fullname.toUpperCase().indexOf(keyWordSearch.toUpperCase()) !== -1 ||
                value.id.toUpperCase().indexOf(keyWordSearch.toUpperCase()) !== -1
        })

        res.json({
            users: newData.slice(start, end),
            totalPage: totalPage
        })
    }
}

module.exports.create = async (req, res) => {
    try {
        const { email, username, password, name, permission } = req.query;
        if (!email) {
            throw new Error("Vui lòng nhập email của bạn !");
        };
        if (!username) {
            throw new Error("Vui lòng nhập username !");
        };
        if (!password) {
            throw new Error("Vui lòng nhập password !");
        };
        if (!name) {
            throw new Error("Vui lòng nhập tên của bạn !");
        };
        const users = await User.find();
        for (const user of users) {
            if (user.email === email || user.username === username) {
                throw new Error("Email hoặc username đã tồn tại");
            };
        };
        const newUser = new User()
        const salt = await bcrypt.genSalt();
        newUser.email = email;
        newUser.username = username;
        newUser.password = await bcrypt.hash(password, salt);
        newUser.fullname = name.toLowerCase().replace(/^.|\s\S/g, a => { return a.toUpperCase() })
        newUser.id_permission = permission;
        newUser.save();
        res.status(201).json({ msg: "Bạn đã thêm thành công" });
    } catch (error) {
        res.send(error.message);
    };
}

module.exports.delete = async (req, res) => {
    const id = req.query.id;

    await User.deleteOne({ _id: id }, (err) => {
        if (err) {
            res.json({ msg: err })
            return;
        }
        res.json({ msg: "Thanh Cong" })
    })

}

module.exports.details = async (req, res) => {
    const user = await User.findOne({ _id: req.params.id });

    res.json(user)
}

module.exports.update = async (req, res) => {
    const user = await User.findOne({ _id: req.query.id });
    if (req.query.email && req.query.email !== user.email) {
        req.query.email = user.email
    }
    if (req.query.username && req.query.username !== user.username) {
        req.query.username = user.username
    }
    if (!req.query.password) {
        req.query.password = user.password;
    } else {
        const salt = await bcrypt.genSalt();
        req.query.password = await bcrypt.hash(req.query.password, salt);
    }

    req.query.name = req.query.name.toLowerCase().replace(/^.|\s\S/g, a => { return a.toUpperCase() })
    await User.updateOne({ _id: req.query.id }, {
        fullname: req.query.name,
        password: req.query.password,
        id_permission: req.query.permission
    }, function (err, res) {
        if (err) return res.json({ msg: err });
    });
    res.json({ msg: "Bạn đã update thành công" })
}

module.exports.login = async (req, res) => {
    try {
        const { email, password } = req.body;
        if (!email) {
            throw new Error("Vui lòng nhập email của bạn !");
        };
        if (!password) {
            throw new Error("Vui lòng nhập mật khẩu của bạn !");
        };
        const user = await User.findOne({ email: email }).populate("id_permission");
        if (!user) {
            throw new Error("Không Tìm Thấy User");
        };
        const isValidPassword = await bcrypt.compare(password, user.password);
        if (!isValidPassword) {
            throw new Error("Sai mật khẩu");
        };
        const token = jwt.sign(
            { _id: user._id },
            process.env.JWT_SECRET,
            { expiresIn: process.env.JWT_EXPIRES_TIME }
        );
        res.status(200).json({ msg: "Đăng nhập thành công", user: user, jwt: token });
    } catch (error) {
        res.send({ msg: error.message });
    };
}
