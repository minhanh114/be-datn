const Users = require('../Models/user')
const Joi = require('joi')
const randomString = require('randomstring')
const bcrypt = require('bcrypt');
const { sendEmailPassword } = require('../Middlewares/mailer');


module.exports.index = async (req, res) => {

    const user = await Users.find()

    res.json(user)

}

module.exports.user = async (req, res) => {

    const id = req.params.id

    const user = await Users.findOne({ _id: id })

    res.json(user)

}

module.exports.detail = async (req, res) => {
    try {
        const { email, password } = req.query;
        if (!email) {
            throw new Error("Vui lòng nhập email của bạn !");
        };
        if (!password) {
            throw new Error("Vui lòng nhập mật khẩu của bạn !");
        };
        const user = await Users.findOne({ email: email });
        if (!user) {
            throw new Error("Khong Tìm Thấy User");
        };
        const invalidPassword = await bcrypt.compare(password, user.password);
        if (!invalidPassword) {
            throw new Error("Sai Mat Khau");
        }
        res.status(200).json(user);
    } catch (error) {
        res.send(error.message);
    }
}

module.exports.post_user = async (req, res) => {
    try {
        const { email, username, password, fullname, id_permission } = req.body;
        if (!email) {
            throw new Error("Vui lòng nhập email của bạn !");
        };
        if (!username) {
            throw new Error("Vui lòng nhập username !");
        };
        if (!password) {
            throw new Error("Vui lòng nhập password !");
        };
        if (!fullname) {
            throw new Error("Vui lòng nhập tên của bạn !");
        };
        const user = await Users.findOne({ email: email });
        const user2 = await Users.findOne({ username: username });
        if (user) {
            throw new Error("Email Da Ton Tai");
        };
        if (user2) {
            throw new Error("Username Da Ton Tai");
        };
        const newUser = new Users();
        newUser.email = email;
        newUser.username = username;
        const salt = await bcrypt.genSalt();
        newUser.password = await bcrypt.hash(password, salt);
        newUser.fullname = fullname.toLowerCase().replace(/^.|\s\S/g, a => { return a.toUpperCase() })
        newUser.id_permission = id_permission;
        await newUser.save();
        res.status(201).json(newUser);
    } catch (error) {
        res.send(error.message);
    }

}

module.exports.update_user = async (req, res) => {
    try {
        const user = await Users.findOne({ _id: req.body._id });
        if (!user) {
            throw new Error('User not found !');
        };
        if (!req.body.oldpassword && !req.body.password) {
            user.fullname = req.body.fullname
            user.username = req.body.username
            await user.save();
            return res.send("Succeed");
        };
        const isValidPassword = await bcrypt.compare(req.body.oldpassword, user.password);
        if (!isValidPassword) {
            throw new Error("Wrong password !");
        };
        const salt = await bcrypt.genSalt();
        user.password = await bcrypt.hash(req.body.password, salt);
        await user.save();
        return res.send("Succeed");
    } catch (error) {
        res.send(error.message);
    }
}

module.exports.forgotPass = async (req, res) => {
    try {
        const schema = Joi.object({ email: Joi.string().email().required(), isClient: Joi.boolean() })
        const { error } = schema.validate(req.body)
        if (error) {
            return res.status(404).json(error.details[0].message)
        }
        const email = req.body.email
        const user = await Users.findOne({ email: email })

        if (!user) {
            return res.status(201).send({ success: false, msg: "Email này không tồn tại trong hệ thống !" })
        }
        let tempClient = false
        if(req.body.isClient === true){
            tempClient = true
        }
        const randomStrings = randomString.generate()
        const data = await Users.findOneAndUpdate({ email: email }, { token: randomStrings })
        sendEmailPassword(user.fullname, user.email, randomStrings, tempClient)
        return res.status(200).send({ success: true, msg: "Vui lòng kiểm tra mail của bạn để khôi phục mật khẩu !" })
    } catch (error) {
        res.send(error.message);
    }
}

module.exports.resetPass = async (req, res) => {
    try {
        const schema = Joi.object({ password: Joi.string().required() });
        const { error } = schema.validate(req.body);
        if (error) {
            return res.status(400).send(error.details[0].message);
        }
        const token = req.query.token
        console.log(token);
        const tokenData = await Users.findOne({ token: token })
        if (tokenData) {
            const salt = await bcrypt.genSalt()
            const newpassword = await bcrypt.hash(req.body.password, salt)
            const user = await Users.findByIdAndUpdate({ _id: tokenData._id }, { $set: { password: newpassword, token: '' } }, { new: true })
            return res.status(201).send({ success: true, msg: "Thay đổi mật khẩu thành công" })
        } else {
            return res.status(201).send({ success: false, msg: "Liên kết này đã hết hạn" })
        }
    } catch (error) {
        return res.status(400).send({ success: false, msg: error.message })
    }
}