const jwt = require('jsonwebtoken')
const User = require('../Models/user')

module.exports.isAutheticatedUser = async (req, res, next) => {
    try {
        const authHeader = req.headers.authorization;
        if (!authHeader) {
            throw new Error("Vui lòng đăng nhập");
        };
        const token = authHeader.slice(1, -1);
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await User.findById(decoded);
        if (!user) {
            throw new Error("Thông tin đăng nhập sai !");
        };
        req.user = user;
        next();
    } catch (error) {
        return res.send({ error: error.message })
    }
};

module.exports.authorizeRoles = (...roles) => {
    return (req, res, next) => {
        if (!roles.includes(req.user.role)) {
            throw new Error(`(${req.user.role}) không có quyền truy cập tài nguyên này`);
        }
        next();
    };
};

