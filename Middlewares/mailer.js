const nodeMailer = require('nodemailer')

const adminEmail = 'anhm6362@gmail.com' // Email

const adminPassword = 'cwykikcgyhlwzyhb' // Password

// Mình sử dụng host của google - gmail
const mailHost = 'smtp.gmail.com'

// 587 là một cổng tiêu chuẩn và phổ biến trong giao thức SMTP
const mailPort = 587

const sendMail = (to, subject, htmlContent) => {
    // Khởi tạo một transporter object sử dụng chuẩn giao thức truyền tải SMTP với các thông tin cấu hình ở trên.
    const transporter = nodeMailer.createTransport({
        host: mailHost,
        port: mailPort,
        secure: false, // nếu các bạn dùng port 465 (smtps) thì để true, còn lại hãy để false cho tất cả các port khác
        auth: {
            user: adminEmail,
            pass: adminPassword
        }
    })

    const options = {
        from: adminEmail, // địa chỉ admin email bạn dùng để gửi
        to: to, // địa chỉ gửi đến
        subject: subject, // Tiêu đề của mail
        html: htmlContent // Phần nội dung mail mình sẽ dùng html thay vì thuần văn bản thông thường.
    }

    // hàm transporter.sendMail() này sẽ trả về cho chúng ta một Promise
    return transporter.sendMail(options, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email đã gửi: ' + info.response);
        }
    }
    );

}

const sendEmailPassword = async (name, email, token, isClient) => {
    try {
        const transporter = nodeMailer.createTransport({
            host: mailHost,
            port: 587,
            secure: false,
            requireTLS: true,
            auth: {
                user: adminEmail,
                pass: adminPassword
            }
        })

        let prefixLink = "http://localhost:3001";
        if (isClient === true) {
            prefixLink = 'http://localhost:3000';
        }

        const mailOptions = {
            from: adminEmail,
            to: email,  //email
            subject: "Khôi phục mật khẩu web thương mại Dimples",
            html: `<p>Xin chào ${name}, Vui lòng đi đến liên kết <a href="${prefixLink}/resetpassword?token=${token}"> sau đây </a>để khôi phục mật khẩu</p>`
        }
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error)
            } else {
                console.log("Mail đã gửi: ", info.response)
            }
        })
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    sendMail: sendMail,
    sendEmailPassword: sendEmailPassword
}

